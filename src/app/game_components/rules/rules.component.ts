import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {
  page;

  constructor(private location: Location) { }

  ngOnInit() {
    this.page = 1;
  }

  goLeft() {
    if (this.page > 1) {
      this.page = this.page - 1;
    }
  }
  goRight() {
    if (this.page < 3) {
      this.page = this.page + 1;
    }
  }

  goBack() {
   this.location.back();
  }
}
