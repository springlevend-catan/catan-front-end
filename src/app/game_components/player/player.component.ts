import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../authentication/service/authenticationService/authentication.service';
import {User} from '../../../model/user';
import {Player} from '../../model/player';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  @Input()
  player;

  @Input()
  avatarNumber;
  private src;

  @Input()
  playerWithTurn;

  constructor(private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.src = '../../assets/avatars/avatar' + this.avatarNumber + '.png';
  }

  calculateResources() {
    let amount = 0;
    for (const key in this.player.resources) {
      amount += this.player.resources[key];
    }
    return amount;
  }
}
