import {Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';

@Component({
  selector: 'app-dice',
  templateUrl: './dice.component.html',
  styleUrls: ['./dice.component.scss']
})
export class DiceComponent implements OnInit {
  @Input()
  die1: number;

  @Input()
  die2: number;

  constructor() {
  }

  ngOnInit() {
  }
}
