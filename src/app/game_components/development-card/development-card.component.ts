import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-development-card',
  templateUrl: './development-card.component.html',
  styleUrls: ['./development-card.component.scss']
})
export class DevelopmentCardComponent implements OnInit {

  @Input() developmentType;
  @Input() developmentValue;
  @Output() playDevelopmentCard: EventEmitter<any> = new EventEmitter<any>();

  private source: string;

  constructor() {
  }

  ngOnInit() {
    console.log('type: ' + this.developmentType);
    console.log('value: ' + this.developmentValue);
    this.getSource();
  }

  private getSource() {
    switch (this.developmentType) {
      case 'INVENTION':
        this.source = '../../../assets/ui_textures/development_assets/Invention.png';
        break;
      case 'KNIGHT':
        this.source = '../../../assets/ui_textures/development_assets/knight.png';
        break;
      case 'STREET_BUILDING':
        this.source = '../../../assets/ui_textures/development_assets/street_building.png';
        break;
      case 'VICTORY':
        this.source = '../../../assets/ui_textures/development_assets/victory.png';
        break;
      case 'YEAR_OF_PLENTY':
        this.source = '../../../assets/ui_textures/development_assets/monopoly.png';
        break;
    }
  }

  playCard(developmentType) {
      this.playDevelopmentCard.emit(developmentType);
  }
}


