import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {BoardService} from '../../../service/boardService/board.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthenticationService} from '../../../authentication/service/authenticationService/authentication.service';
import {TranslateService} from '../../../service/translateService/translate.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  private hexWidthMax;
  private hexWidth;
  private hexWidthMin;
  private gridSize;
  private horizontalMargin;

  @Input()
  public colourArray;
  // private board; --> made public to check CI/CD, was giving error here
  @Input()
  public board;

  @Input()
  public isBuildingCity = false;

  @Input() robber: number[];

  @Input() movingRobber: boolean;

  @Output()
  moveRobberEvent = new EventEmitter();

  @Output()
  buildVillageEvent = new EventEmitter();

  @Output()
  buildStreetEvent = new EventEmitter();

  @Output()
  buildCityEvent = new EventEmitter();

  textureUrls = [];

  constructor(private sanitazer: DomSanitizer, private authService: AuthenticationService, private translate: TranslateService) {
  }

  ngOnInit() {
    this.hexWidthMax = 45;
    this.hexWidthMin = 25;
    // width of a hexagon - used by the hex component
    this.calculateHexWidth(window.innerWidth / 20);

    // amount of hexagons in the middle of the grid
    this.gridSize = 7;
    // margin is used to add a left margin and create an offset
    this.horizontalMargin = 5;
    // relative paths to the textures in the assets folder
    this.textureUrls = ['.../../assets/textures/stone.png', '../../assets/textures/erts.png', '../../assets/textures/water.png', '../../assets/textures/wheat.png', '../../assets/textures/wood.png', '../../assets/textures/wool.png', '../../assets/textures/desert.png'];
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const size = event.target.innerWidth / 20;
    this.calculateHexWidth(size);
  }

  buildVillage(data) {
    console.log(data);
    this.buildVillageEvent.emit(data);
  }

  buildStreet(data) {
    console.log(data);
    this.buildStreetEvent.emit(data);
  }

  buildCity(data) {
    console.log(data);
    this.buildCityEvent.emit(data);
  }

  moveRobber(data) {
    this.moveRobberEvent.emit(data);
  }

  calculateHexWidth(size) {
    if (size > this.hexWidthMax) {
      this.hexWidth = this.hexWidthMax;
    } else if (size < this.hexWidthMin) {
      this.hexWidth = this.hexWidthMin;
    } else if (size < this.hexWidthMax) {
      this.hexWidth = size;
    }
  }

  calculateMargin(i: number) {
    const halfCount = Math.floor(this.gridSize / 2);
    return {
      'margin-left': Math.abs(halfCount - i) * (((this.hexWidth) * 1.7 / 2) + this.horizontalMargin / 2) + 'px',
      'margin-bottom': -this.hexWidth / 2 + 'px'
    };
  }

  // calculates the amount of hexagons that should be generated on a row depending on the size of the grid
  calculateRowSize(i: number) {
    const halfCount = Math.floor(this.gridSize / 2);
    return this.gridSize - Math.abs(halfCount - i);
  }

  // helper function to use with ngFor because you can only loop over an arrat
  amount(n: number) {
    return Array(n);
  }

  // gets the texture from the assets based on the string returned from the api
  getTexture(type: string) {
    let url: string;
    switch (type) {
      case 'BRICK':
        url = this.textureUrls[0];
        break;
      case 'WATER' :
        url = this.textureUrls[2];
        break;
      case 'PORT' :
        url = this.textureUrls[2];
        break;
      case 'PORT_BRICK' :
        url = this.textureUrls[2];
        break;
      case 'PORT_WOOD' :
        url = this.textureUrls[2];
        break;
      case 'PORT_WOOL' :
        url = this.textureUrls[2];
        break;
      case 'PORT_ORE' :
        url = this.textureUrls[2];
        break;
      case 'PORT_GRAIN' :
        url = this.textureUrls[2];
        break;
      case 'GRAIN' :
        url = this.textureUrls[3];
        break;
      case 'ORE' :
        url = this.textureUrls[1];
        break;
      case 'WOOD' :
        url = this.textureUrls[4];
        break;
      case 'WOOL' :
        url = this.textureUrls[5];
        break;
      case 'DESERT' :
        url = this.textureUrls[6];
        break;
    }

    return {
      'background-image': 'url(' + url.toString() + ')',
      'background-size': 'auto ' + this.hexWidth * 2 + 'px'
    };
  }
}
