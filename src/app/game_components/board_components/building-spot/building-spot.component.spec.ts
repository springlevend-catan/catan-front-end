import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingSpotComponent } from './building-spot.component';

describe('BuildingSpotComponent', () => {
  let component: BuildingSpotComponent;
  let fixture: ComponentFixture<BuildingSpotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingSpotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
