import {Component, Input, OnInit} from '@angular/core';
import index from '@angular/cli/lib/cli';

@Component({
  selector: 'app-building-spot',
  templateUrl: './building-spot.component.html',
  styleUrls: ['./building-spot.component.scss']
})
export class BuildingSpotComponent implements OnInit {

  @Input() index;
  @Input() x;
  @Input() y;
  @Input() isVisible;
  @Input() colour;
  @Input() type;
  @Input() hexWidth;
  @Input() isBuildingCity;
  @Input() username;
  private coordinates;

  constructor() {
  }

  ngOnInit() {
  }

  isBuildingof() {
    return this.username === localStorage.getItem('Username');
  }

  calculateSize() {
    if (this.type === 1 || this.type === 5) {
      let rotate;
      switch (this.index) {
        case 0:
        case 3:
          rotate = -30;
          break;
        case 1:
        case 4:
          rotate = 30;
          break;
        default:
          rotate = 90;
      }
      return {
        'height': this.hexWidth / 5 + 'px',
        'width': this.hexWidth * 4 / 5 + 'px',
        'transform': 'rotate(' + rotate + 'deg)'
      };
    }
    if (this.type === 2 || this.type === 4) {
      return {
        'width': (this.hexWidth / 3) + 'px',
        'height': 'auto',
        'left': (-this.hexWidth / 3 / 3) + 'px',
        'top': (-this.hexWidth * 1.7 / 3 / 3) + 'px'
      };
    }
    if (this.type === 3) {
      return {
        'width': (this.hexWidth / 2) + 'px',
        'height': 'auto',
        'left': (-this.hexWidth / 3 / 2) + 'px',
        'top': (-this.hexWidth * 1.7 / 3 / 2) + 'px'
      };
    }
  }

  getAsset() {
    if (this.type === 2) {
      return '../../assets/board_pieces/village_' + this.colour + '.png';
    }
    if (this.type === 3) {
      return '../../assets/board_pieces/city_' + this.colour + '.png';
    }

    if (this.type === 4) {
      return '../../assets/board_pieces/village_99.png';
    }
  }
}
