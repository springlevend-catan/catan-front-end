import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-hex',
  templateUrl: './hex.component.html',
  styleUrls: ['./hex.component.scss']
})
export class HexComponent implements OnInit {

  // width of the hexagon
  @Input() width;
  // type of the tile
  @Input() type;
  // value of the tile
  @Input() number;
  // x coordinate
  @Input() x;
  // y coordinate
  @Input() y;
  // size of the grid
  @Input() gridSize;
  // size of the row the tile is on
  @Input() rowSize;

  @Input() isBuildingCity: boolean;

  @Input() buildings;

  @Input() colourArray;

  @Input() isRobber;

  @Input() movingRobber;

  @Output()
  moveRobberEvent = new EventEmitter();
  @Output()
  buildVillageEvent = new EventEmitter();
  @Output()
  buildStreetEvent = new EventEmitter();
  @Output()
  buildCityEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  // calculates the position of the buildingspots based on the width
  // type 0 : villages/towns - only the corners
  // type 1 : streets - sides
  calculatePosition(buildingType, index) {
    let left = 0;
    let top = 0;
    let type;

    if (buildingType === 'STREET' || buildingType === 'STREET_BUILDING_SPOT') {
      type = 1;
    } else {
      type = 0;
    }

    if (type === 0) {
      switch (index) {
        case 0:
          left = 0;
          top = this.width / 2;
          break;
        case 1:
          left = this.width * 1.7 / 2;
          top = 0;
          break;
        case 2:
          left = this.width * 1.7; // 40 85
          top = this.width / 2;
          break;
        case 3:
          left = this.width * 1.7;
          top = 3 * this.width / 2;
          break;
        case 4:
          left = this.width * 1.7 / 2;
          top = this.width * 2;
          break;
        case 5:
          left = 0;
          top = this.width * 3 / 2;
          break;
      }
    } else {
      switch (index) {
        case 0:
          left = this.width * 1.7 / 4;
          top = this.width / 4 - this.width / 20;
          break;
        case 1:
          left = this.width * 1.7 * 3 / 4 + this.width / 20;
          top = this.width / 4 - this.width / 20;
          break;
        case 2:
          left = this.width * 1.7 + this.width / 20;
          top = this.width;
          break;
        case 3:
          left = this.width * 1.7 * 3 / 4 + this.width / 20;
          top = this.width * 7 / 4 + this.width / 20;
          break;
        case 4:
          left = this.width * 1.7 / 4;
          top = this.width * 7 / 4 + this.width / 20;
          break;
        case 5:
          left = 0 - this.width / 20;
          top = this.width;
          break;
      }
      // Correction move the middle of the street over the middle of the side after transformation
      left -= this.width * (4 / 5) / 2;
      top -= this.width / 10;
    }

    return {
      'left': left + 'px',
      'top': top + 'px'
    };
  }

  // prevents showing double buildingspots in one place
  buildingIsInHex(buildingType, index) {
    let type;
    if (buildingType === 'STREET' || buildingType === 'STREET_BUILDING_SPOT') {
      type = 1;
    } else {
      type = 0;
    }

    if (type === 0) {
      switch (index) {
        case 0:
          return this.y === 1 && this.x !== 0 && this.x < this.gridSize / 2;
        case 1:
          return this.x === 1 && this.y !== 0;
        case 2:
          return (this.x === 1 || this.y === this.rowSize - 2) && this.x < this.gridSize / 2;
        case 3:
          return true;
        case 4:
          return this.y !== 0 && this.y !== this.rowSize - 1;
        case 5:
          return this.y === 1;
      }
    } else {
      switch (index) {
        case 0:
          return this.x < this.gridSize / 2 && (this.y === 1 || this.x === 1);
        case 1:
          return this.x === 1 || (this.y === this.rowSize - 2) && (this.x <= this.gridSize / 2);
        case 5:
          return this.y === 1;
        default:
          return true;
      }
    }
  }

  amount(n: number) {
    return Array(n);
  }

  showBuildingCoord(number: number, i: number) {
    console.log('(' + this.x + ', ' + this.y + ') - ' + i + ' ' + (number === 0 ? 'building' : 'street'));
  }


  build(buildingType: string, i: number) {
    const x = this.x;
    const y = this.y;
    const data = JSON.stringify({/*number: number,*/ index: i, x: x, y: y});
    console.log('hex ' + data);
    if (buildingType === 'STREET_BUILDING_SPOT') {
      this.buildStreetEvent.emit(data);
    }
    if (buildingType === 'VILLAGE_BUILDING_SPOT') {
      this.buildVillageEvent.emit(data);
    }
    if (buildingType === 'VILLAGE' && this.isBuildingCity) {
      this.buildCityEvent.emit(data);
    }

  }

  getPlayerColour(i: number, buildingType: string) {
    if (buildingType === 'STREET_BUILDING_SPOT') {
      return 'rgba(255,255,255,0)';
    }
    if (buildingType === 'STREET') {
      return this.colourArray[i];
    } else {
      return i;
    }

  }

  getBuildingType(buildingType: string) {
    switch (buildingType) {
      case 'EMPTYSPOT':
        return 0;
      case 'STREET':
        return 1;
      case 'VILLAGE':
        return 2;
      case 'CITY':
        return 3;
      case 'VILLAGE_BUILDING_SPOT':
        return 4;

      case  'STREET_BUILDING_SPOT':
        return 5;
    }
  }

  isWater() {
    return this.x !== 0 && this.y !== 0 && this.x !== this.gridSize - 1 && this.y !== this.rowSize - 1;
  }

  moveRobber() {
    const x = this.x;
    const y = this.y;
    const data = JSON.stringify({x: x, y: y});
    console.log('robber' + data);
    this.moveRobberEvent.emit(data);
  }
}
