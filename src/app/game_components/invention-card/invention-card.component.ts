import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-invention-card',
  templateUrl: './invention-card.component.html',
  styleUrls: ['./invention-card.component.scss']
})
export class InventionCardComponent implements OnInit {


  @Input() player;

  // ORDER: Brick, Grain , Ore, Wood, Wool
  private resources = [];

  @Output() doInventionAction: EventEmitter<any> = new EventEmitter<any>();


  constructor() {
  }

  ngOnInit() {
    this.resources[0] = 0;
    this.resources[1] = 0;
    this.resources[2] = 0;
    this.resources[3] = 0;
    this.resources[4] = 0;
  }

  getValue(key: any) {
    switch (key) {
      case 'BRICK':
        return this.resources[0];
      case 'GRAIN':
        return this.resources[1];
      case 'ORE':
        return this.resources[2];
      case 'WOOD':
        return this.resources[3];
      case 'WOOL':
        return this.resources[4];
    }
  }

  increaseValue(resource) {
    switch (resource) {
      case 'BRICK':
        this.resources[0] = this.resources[0] + 1;
        break;
      case 'GRAIN':
        this.resources[1] = this.resources[1] + 1;
        break;
      case 'ORE':
        this.resources[2] = this.resources[2] + 1;
        break;
      case 'WOOD':
        this.resources[3] = this.resources[3] + 1;
        break;
      case 'WOOL':
        this.resources[4] = this.resources[4] + 1;
        break;
    }
  }

  decreaseValue(resource) {
    switch (resource) {
      case 'BRICK':
        this.resources[0] = this.resources[0] - 1;
        break;
      case 'GRAIN':
        this.resources[1] = this.resources[1] - 1;
        break;
      case 'ORE':
        this.resources[2] = this.resources[2] - 1;
        break;
      case 'WOOD':
        this.resources[3] = this.resources[3] - 1;
        break;
      case 'WOOL':
        this.resources[4] = this.resources[4] - 1;
        break;
    }
  }

  calculateTotal() {
    return this.resources[0] + this.resources[1] + this.resources[2] + this.resources[3] + this.resources[4];
  }

  getInventionResources() {
    this.doInventionAction.emit(this.resources);
  }

}
