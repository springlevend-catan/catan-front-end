import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventionCardComponent } from './invention-card.component';

describe('InventionCardComponent', () => {
  let component: InventionCardComponent;
  let fixture: ComponentFixture<InventionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
