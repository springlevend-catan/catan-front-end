import {Component, ViewEncapsulation} from '@angular/core';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import * as $ from 'jquery';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {LocalStorageService} from '../../service/local-storage-service/local-storage.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  // Dynamically dding a chat message to DOM doesn't apply styling; adding ViewEncapsulation.None solves this problem
  encapsulation: ViewEncapsulation.None
})
export class ChatComponent {
  // Chat ID
  private id: string;

  // WebSocket URL
  private url = 'http://localhost:5500/chat-socket';

  // WebSocket client
  private stompClient;

  // Connected players (displayed in chat's side bar so people know who's connected)
  private players = [];

  // Chat message value
  private value: string;

  constructor(private route: ActivatedRoute, private localStorageService: LocalStorageService) {
    // ID provided in URL; will have to be serviced by lobby when we put everything together
    this.id = this.route.snapshot.paramMap.get('url');

    // Initialize WebSocket connection
    this.initializeWebSocketConnection();
  }

  // Format a chat message sent by user (so no join or leave messages)
  static formatMessage(sender: string, message: string, date: string): string {
    return `<b>${sender} [${moment(date).format('HH:mm:ss')}]:</b> ${message}`;
  }

  initializeWebSocketConnection(): void {
    if (this.id == null) {
      return;
    }

    // Create socket and send chat ID and token of connected user
    console.log(`${this.url}?token=${this.localStorageService.getItem('token')}&id=${this.id}`);
    const socket = new SockJS(`${this.url}?token=${this.localStorageService.getItem('token')}&id=${this.id}`);

    // Keep reference to this for usage within stompClient.connect() and stompClient.subscribe()
    const that = this;

    // Initialize STOMP client for socket
    this.stompClient = Stomp.over(socket);

    // Connect client
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe(`/topic/chat/${that.id}`, (r) => {
        if (r.body) { // If a message arrives...
          // ... parse it to JSON
          const response = JSON.parse(r.body);

          // Reference to our chat window
          const chatWindow = $('.chat-window');

          if (response.type === 'join') { // If it's a join message, add message to chat, and refresh the list of connected players
            chatWindow.append(`<div class="join-leave-message">${response.username} has joined chat ${that.id}.</div>`);

            that.players = response.chat.players;
          } else if (response.type === 'leave') { // If it's a leave message, add message to chat, and refresh the list of connected players
            chatWindow.append(`<div class="join-leave-message">${response.username} has left chat ${that.id}.</div>`);

            that.players = response.chat.players;
          } else { // If it's neither a join or leave message, it's a chat message, so apply formatting and add to chat
            chatWindow.append(`<div class="chat-message">${ChatComponent.formatMessage(response.message.sender, response.message.message, response.message.time)}</div>`);
          }

          // Scroll to bottom of chat
          chatWindow.scrollTop(function () {
            return this.scrollHeight;
          });
        }
      });
    });
  }

  // Called on keydown in chat input field
  sendMessageWithEnter(event: any): void {
    // if (event.key === 'Enter' && event.target.value.replace(/\s/g, '').length) { // If the pressed key is Enter and if the message isn't empty...
    if (event.key === 'Enter' && this.value.length) {
      this.sendMessage();
    }
  }

  sendMessage() {
    this.stompClient.send(`/app/send-message/${this.localStorageService.getItem('token')}/${this.id}`, {}, this.value); // ... send it

    this.value = ''; // Clear chat input field
  }
}
