import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StealComponent } from './steal.component';

describe('StealComponent', () => {
  let component: StealComponent;
  let fixture: ComponentFixture<StealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
