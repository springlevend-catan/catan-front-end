import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LobbyService} from '../../service/lobbyService/lobby.service';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {ToastrService} from 'ngx-toastr';
import {TradeRequest} from '../../model/tradeRequest';
import {GameService} from '../../service/gameService/game.service';
import {ResourceTransaction} from '../../model/resourceTransaction';
import {Action} from '../../model/action';
import {NotificationService} from '../../service/notification-service/notification.service';
import {AuthenticationService} from '../../authentication/service/authenticationService/authentication.service';
import {User} from '../../../model/user';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  game = null;
  stompClient;
  url;
  isBuildingCity: boolean;
  token;
  colourArray;
  player;
  hideBuildActions: boolean;
  trading = false;
  tradeRequest: TradeRequest;
  audio;
  completed;

  private streetBuildData;
  private showYearOfPlentyMenu = false;
  private showDevelopmentCardMenu = false;
  private showInventionMenu = false;
  private inventionResources = [];
  private avatars = [];
  private stealResourcePlayers = [];
  showStealMenu: boolean;

  constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private toastr: ToastrService, private lobbyService: LobbyService, private router: Router, private gameService: GameService, private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.setupAudio();
    this.colourArray = ['#FFFFFF', '#FFFF00', '#00FF00', '#0000FF'];
    this.hideBuildActions = true;
    this.avatars = [];

    this.token = 'Bearer ' + localStorage.getItem('token');

    // Get paramater from the url
    this.url = this.route.snapshot.paramMap.get('url');
    // Testing visualisation
    // TODO: refactor socket to service
    if (this.url === null) {
      const user = 'Player1';
      this.game = '{"url":"-521175720","tiles":[[{"x":0,"y":0,"tileType":"PORT_WOOL","number":0,"buildings":[]},{"x":0,"y":1,"tileType":"WATER","number":0,"buildings":[]},{"x":0,"y":2,"tileType":"PORT_WOOD","number":0,"buildings":[]},{"x":0,"y":3,"tileType":"WATER","number":0,"buildings":[]}],[{"x":1,"y":0,"tileType":"PORT_ORE","number":0,"buildings":[]},{"x":1,"y":1,"tileType":"ORE","number":4,"buildings":[{"buildingType":"STREET","index":0,"playerOrder":0},{"buildingType":"STREET","index":1,"playerOrder":1},{"buildingType":"STREET","index":2,"playerOrder":1},{"buildingType":"STREET","index":5,"playerOrder":1}]},{"x":1,"y":2,"tileType":"BRICK","number":6,"buildings":[]},{"x":1,"y":3,"tileType":"GRAIN","number":9,"buildings":[]},{"x":1,"y":4,"tileType":"WATER","number":0,"buildings":[]}],[{"x":2,"y":0,"tileType":"WATER","number":0,"buildings":[]},{"x":2,"y":1,"tileType":"WOOL","number":2,"buildings":[]},{"x":2,"y":2,"tileType":"WOOD","number":5,"buildings":[]},{"x":2,"y":3,"tileType":"BRICK","number":12,"buildings":[{"buildingType":"VILLAGE","index":4,"playerOrder":0}]},{"x":2,"y":4,"tileType":"WOOD","number":4,"buildings":[{"buildingType":"VILLAGE","index":3,"playerOrder":1}]},{"x":2,"y":5,"tileType":"PORT","number":0,"buildings":[]}],[{"x":3,"y":0,"tileType":"PORT","number":0,"buildings":[]},{"x":3,"y":1,"tileType":"ORE","number":9,"buildings":[{"buildingType":"STREET","index":4,"playerOrder":1}]},{"x":3,"y":2,"tileType":"GRAIN","number":8,"buildings":[]},{"x":3,"y":3,"tileType":"WOOD","number":8,"buildings":[]},{"x":3,"y":4,"tileType":"WOOL","number":10,"buildings":[{"buildingType":"VILLAGE","index":4,"playerOrder":0}]},{"x":3,"y":5,"tileType":"WOOD","number":3,"buildings":[]},{"x":3,"y":6,"tileType":"WATER","number":0,"buildings":[]}],[{"x":4,"y":0,"tileType":"WATER","number":0,"buildings":[]},{"x":4,"y":1,"tileType":"GRAIN","number":5,"buildings":[]},{"x":4,"y":2,"tileType":"GRAIN","number":10,"buildings":[]},{"x":4,"y":3,"tileType":"BRICK","number":11,"buildings":[]},{"x":4,"y":4,"tileType":"ORE","number":3,"buildings":[{"buildingType":"CITY","index":3,"playerOrder":0}]},{"x":4,"y":5,"tileType":"PORT","number":0,"buildings":[]}],[{"x":5,"y":0,"tileType":"PORT","number":0,"buildings":[]},{"x":5,"y":1,"tileType":"WOOL","number":6,"buildings":[{"buildingType":"STREET","index":3,"playerOrder":1}]},{"x":5,"y":2,"tileType":"WOOL","number":11,"buildings":[]},{"x":5,"y":3,"tileType":"DESERT","number":0,"buildings":[]},{"x":5,"y":4,"tileType":"WATER","number":0,"buildings":[]}],[{"x":6,"y":0,"tileType":"WATER","number":0,"buildings":[]},{"x":6,"y":1,"tileType":"PORT_GRAIN","number":0,"buildings":[]},{"x":6,"y":2,"tileType":"WATER","number":0,"buildings":[]},{"x":6,"y":3,"tileType":"PORT_BRICK","number":0,"buildings":[]}]],"players":[{"username":"Player1","points":0,"longestRoad":0,"numberOfKnights":0,"turnOrder":0,"colorId":0,"resources":{"GRAIN":3,"WOOD":0,"ORE":5,"BRICK":0,"WOOL":1},"active":true,"host":true},{"username":"Player2","points":0,"longestRoad":0,"numberOfKnights":0,"turnOrder":1,"colorId":1,"resources":{"GRAIN":3,"WOOD":1,"ORE":0,"BRICK":6,"WOOL":0},"active":true,"host":false}, {"username":"Player3","points":0,"longestRoad":0,"numberOfKnights":0,"turnOrder":1,"colorId":1,"resources":{"GRAIN":3,"WOOD":1,"ORE":0,"BRICK":6,"WOOL":0},"active":true,"host":false}, {"username":"Player4","points":0,"longestRoad":0,"numberOfKnights":0,"turnOrder":1,"colorId":1,"resources":{"GRAIN":3,"WOOD":1,"ORE":0,"BRICK":6,"WOOL":0},"active":true,"host":false}],"requiredAction":"","lastAction":null,"playerWithTurn":{"username":"Player3","points":0,"longestRoad":0,"numberOfKnights":0,"turnOrder":0,"colorId":0,"resources":{"GRAIN":3,"WOOD":0,"ORE":5,"BRICK":0,"WOOL":1},"active":true,"host":true},"robber":[3,4]}';
      this.game = JSON.parse(this.game);
      // TODO: Get User from token then move this function out of this if statement
      this.player = this.getPlayerData(user);
      console.log(this.game.tiles);
    } else {
      console.log('init');

      // Create new Socket
      const socket = new SockJS('http://localhost:5500/ws');
      this.stompClient = Stomp.over(socket);
      console.log('create socket');
      const that = this;
      // Connect the socket to the correct endpoint
      this.stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        that.stompClient.subscribe('/topic/game/' + that.url, function (msg) {
          if (msg.body.toString().startsWith('Exception')) {
            that.toastr.error('Error : ' + msg.body.toString());
            that.tradeRequest = null;
          }
          console.log('game wordt geupdated ! ');
          that.tradeRequest = null;
          that.game = JSON.parse(msg.body);
          that.player = that.getPlayerData(localStorage.getItem('Username'));
          that.completed = that.game.completed;
          that.visualizeState();

          console.log(that.game);

          if (that.avatars.length === 0) {
            that.initialiseAvatars();
          }

        });
        that.joinPlayer();

      });
    }
  }

  initialiseAvatars() {
    console.log('INITIALISING AVATARS');
    for (let i = 0; i < this.game.players.length; i++) {
     if (this.game.players[i].username.startsWith('Computer')) {
       this.avatars[i] = Math.floor(Math.random() * 10);
     } else {
       this.authenticationService.getUserByUserName(this.game.players[i].username).subscribe(x => {
         this.avatars[i] = x.profilePicture;
       });
     }
     // this.authenticationService.getUserByUserName(this.game.players[i].username).subscribe(x => {
     //     this.avatars[i] = x.profilePicture;
     // });
    }
  }

  setupAudio() {
    this.audio = new Audio();
    this.audio.src = '../../assets/audio/soundtrack.wav';
    this.audio.load();
    this.audio.loop = true;
    this.audio.play();
  }

  toggleAudio() {
    if (this.audio.paused) {
      this.audio.play();
    } else {
      this.audio.pause();
    }
  }

  visualizeState() {
    if (this.game.playerWithTurn.username === localStorage.getItem('Username')) {
      if (this.game.requiredAction === 'BUILD_VILLAGE') {
        console.log('Please build a village');
        this.popUpBuildings();
      }
      if (this.game.requiredAction === 'BUILD_STREET' || this.game.requiredAction === 'BUILD_TWO_STREETS' || this.game.requiredAction === 'BUILD_SECOND_STREET') {
        this.popUpStreets();
        console.log('Build a street');
      }
      if (this.game.requiredAction === 'ROBBER_STEAL_RESOURCE') {
        this.indicateStealPlayers();
      }
    }
    if (this.game.playerWithTurn.computer) {
      this.playComputer(this.game.playerWithTurn.username);
    }
  }

  popUpDevelopmentCards() {
    this.showDevelopmentCardMenu = !this.showDevelopmentCardMenu;
  }

  playCard(type: any) {
    this.showDevelopmentCardMenu = false;
    switch (type) {
      case 'KNIGHT':
        this.playKnightCard();
        break;
      case 'VICTORY':
        this.playVictoryCard();
        break;
      case 'YEAR_OF_PLENTY':
        this.showYearOfPlentyCards();
        break;
      case 'STREET_BUILDING':
        this.playStreetBuildingCard();
        break;
      case 'INVENTION':
        this.showInventionCards();
        break;
    }
  }

  private playComputer(username: string) {
    setTimeout(() => this.stompClient.send('/app/game/playComputer/' + this.url, {}, username, 1000));
  }

  private joinPlayer() {
    this.stompClient.send('/app/game/join/' + this.url, {}, this.token);
  }

  buildVillage(data: any) {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'BUILD_VILLAGE', data)));
  }

  buildStreet(data: any) {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'BUILD_STREET', data)));
  }

  buildCity(data: any) {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'BUILD_CITY', data)));
    this.isBuildingCity = false;
  }

  buyDevelopmentCard() {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'BUY_DEVELOPMENT_CARD', '')));
  }

  playVictoryCard() {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'PLAY_VICTORY_CARD', '')));
  }

  playKnightCard() {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'PLAY_KNIGHT_CARD', '')));
  }

  playYearOfPlentyCard(resource: any) {
    this.showYearOfPlentyMenu = false;
    // Data = the resource that has to be removed from every player and added to the active player
    const data = JSON.stringify({resourceToGet: resource});
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'PLAY_YEAR_OF_PLENTY_CARD', data)));
  }

  showYearOfPlentyCards() {
    if (this.showYearOfPlentyMenu) {
      this.showYearOfPlentyMenu = false;
    } else {
      this.showYearOfPlentyMenu = true;
    }
  }

  playInventionCard($event) {
    this.showInventionMenu = false;
    const data = JSON.stringify({resourcesToGet: $event});
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'PLAY_INVENTION_CARD', data)));
  }

  playStreetBuildingCard() {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'PLAY_STREET_BUILDING', '')));
  }

  private showInventionCards() {
    if (this.showInventionMenu) {
      this.showInventionMenu = false;
    } else {
      this.showInventionMenu = true;
    }
  }

  // When clicked, the fake buildings will pop up on the screen
  popUpBuildings() {
    this.gameService.getBuildingSpots(this.url).subscribe(x => {
      this.game.tiles = x;
    });
  }

  popUpStreets() {
    this.gameService.getStreetSpots(this.url).subscribe(x => {
        console.log(x);
        this.game.tiles = x;
      }
    );
  }

  indicateStealPlayers() {
    this.gameService.getStealPlayers(this.url).subscribe(x => {
      for (const player of this.game.players) {
        const response = x as string[];
        console.log(response);
        console.log(response.includes(player.username));
        if (response.includes(player.username)) {
          this.stealResourcePlayers.push(player);
        }
      }
    });
    this.showStealMenu = true;
  }

  moveRobber(data: any) {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'MOVE_ROBBER', data)));
  }

  rollDice() {
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'DICE', '')));
  }

  showBuildActions() {
    this.hideBuildActions = !this.hideBuildActions;
  }

  getPlayerData(user: string) {
    return this.game.players.filter(p => p.username === user)[0];
  }

  endTurn() {
    this.hideBuildActions = true;
    this.trading = false;
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'END_TURN', '')));
  }

  popUpCities() {
    this.isBuildingCity = true;
  }

  trade() {
    console.log('aangeklikt');
    console.log(!this.trading);
    this.trading = !this.trading;
  }


  receivedRequest(request: TradeRequest) {
    this.tradeRequest = request;
    console.log('receiving resources : ');
  }

  sentOutTradeRequest(event: boolean) {
    this.trading = event;
    console.log('changed trading attribute : ' + this.trading);
  }

  doTradeAction() {
    const data = JSON.stringify(new ResourceTransaction(this.tradeRequest.username.toString(), this.player.username, this.tradeRequest.resourcesToGet, this.tradeRequest.resourcesToGive));
    console.log('sending away...');
    console.log(data);
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'RESOURCE_TRANSACTION', data)));
  }
 refuseAction() {
    this.tradeRequest = null;
  }
  
  robberSteal(data) {
    const takeFrom = data;
    const giveTo = localStorage.getItem('Username');
    const sendData = JSON.stringify({takeFrom: takeFrom, giveTo: giveTo});
    this.stompClient.send('/app/game/action/' + this.url, {}, JSON.stringify(new Action(this.token, 'ROBBER_STEAL_RESOURCE', sendData)));
    this.showStealMenu = false;
    this.stealResourcePlayers = [];
  }
}
