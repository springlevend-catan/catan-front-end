import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from '../../model/player';
import {Stomp} from '@stomp/stompjs';

import * as SockJS from 'sockjs-client';
import {ActivatedRoute} from '@angular/router';
import {Action} from '../../model/action';
import {TradeRequest} from '../../model/tradeRequest';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.scss']
})
export class TradeComponent implements OnInit {
  stompClient;
  @Input()
  player: Player;
  @Input()
  game;
  @Output()
  request: EventEmitter<TradeRequest> = new EventEmitter();
  url;
  resourcesToGive = [];
  resourcesToGet = [];
  /**
   * This will send a signal to the parent that you have sent the trade request, so the trade menu will slide back in. Sending out false wille close the menu,
   * and sending out true will open it.
   */
  @Output()
  hasTraded: EventEmitter<boolean> = new EventEmitter();

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.resourcesToGive[0] = 0;
    this.resourcesToGive[1] = 0;
    this.resourcesToGive[2] = 0;
    this.resourcesToGive[3] = 0;
    this.resourcesToGive[4] = 0;
    this.resourcesToGet[0] = 0;
    this.resourcesToGet[1] = 0;
    this.resourcesToGet[2] = 0;
    this.resourcesToGet[3] = 0;
    this.resourcesToGet[4] = 0;
    const socket = new SockJS('http://localhost:5500/ws');
    this.url = this.route.snapshot.paramMap.get('url');
    this.stompClient = Stomp.over(socket);
    const that = this;
    // Connect the socket to the correct endpoint
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/topic/trade/' + that.url, function message(msg) {
        that.receiveTradeRequest(msg);
      });
    });
  }

  sendTradeRequest() {
    console.log('to get : ' + this.resourcesToGet[0]);
    this.hasTraded.emit(false);
    const tradeRequest = new TradeRequest(this.resourcesToGive, this.player.username, this.resourcesToGet);
    console.log('wordt verstuurd : ' + JSON.stringify(tradeRequest));
    this.stompClient.send('/app/game/sendTradeRequest/' + this.url, {}, JSON.stringify(tradeRequest));
  }

  receiveTradeRequest(msg) {
    console.log('message received ...');
    const tradeRequest = JSON.parse(msg.body);
    if (tradeRequest.username !== this.player.username) {
      this.request.emit(tradeRequest);
    }

  }

  changeResourceToGive(event, index) {
    this.resourcesToGive[index] = event.target.value;
  }

  changeResourceToGet(event, index) {
    console.log('u krijgt nog een resource, namelijk deze hoeveelhei : ' + event.target.value);
    this.resourcesToGet[index] = event.target.value;
    console.log(index);
    console.log('hij steekt erin!!!!!!!!' + this.resourcesToGet[index]);
  }
}
