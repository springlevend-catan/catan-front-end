import * as $ from 'jquery';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {LocalStorageService} from '../service/local-storage-service/local-storage.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationHandler {
  private token = this.localStorageService.getItem('token');

  constructor(private httpClient: HttpClient, private localStorageService: LocalStorageService, private router: Router) {
  }

  private notificationTypes = [
    {
      type: 'friend request', message: '%0 has sent you a friend request.', title: 'Friend request', form: {
        'buttons': [
          {
            text: 'Accept', clickMethod: (id) => {
              this.httpClient.post(`http://localhost:5500/user/${this.token}/add-friend?accepted=true&id=${id}`, null, {responseType: 'text'}).subscribe((r) => {
                console.log(r);
              });
            }
          },
          {
            text: 'Decline', clickMethod: (id) => {
              this.httpClient.post(`http://localhost:5500/user/${this.token}/add-friend?accepted=false&id=${id}`, null, {responseType: 'text'}).subscribe((r) => {
                console.log(r);
              });
            }
          }
        ]
      }
    },
    {
      type: 'game invitation', message: '%0 has invited you to a game.', title: 'Game invitation', form: {
        'buttons': [
          {
            text: 'Accept', clickMethod: (id) => {
              this.httpClient.post(`http://localhost:5500/user/${this.token}/join-game?accepted=true&id=${id}`, null, {responseType: 'text'}).subscribe((r) => {
                this.router.navigateByUrl(`lobby/${r}`);
              });
            }
          },
          {
            text: 'Decline', clickMethod: (id) => {
              this.httpClient.post(`http://localhost:5500/user/${this.token}/join-game?accepted=false&id=${id}`, null).subscribe();
            }
          }
        ]
      }
    }
  ];

  /**
   * Creates and returns an element containing a notification.
   *
   * @param container The jQuery element that should contain the message and its buttons
   * @param notification The notification
   * @param showButtons Whether or not to display buttons
   * @param buttonClassNames The names of the classes to be applied to the buttons (if showButtons === true)
   */
  createNotificationElement(container, notification, showButtons?, buttonClassNames?) {
    const type = this.getNotificationType(notification.type);

    if (type == null) {
      return;
    }

    const message = this.formatByFormatString(type.message, notification.parameters);

    container.append(message);

    if (showButtons === true) {
      const buttonContainer = $('<div></div>');
      const buttons = type.form.buttons;

      for (let i = 0; i < buttons.length; i++) {
        const button = $(`<button>${buttons[i].text}</button>`);

        for (let j = 0; j < buttonClassNames.length; j++) {
          button.addClass(buttonClassNames[j]);
        }

        button.click(() => {
          buttons[i].clickMethod(notification.id);

          container.remove();
        });

        buttonContainer.append(button);
      }

      container.append(buttonContainer);
    }

    return container;
  }

  /**
   * Replaces the format items in the message by a parameter with a matching index (e.g. format item %0 is replaced by parameter with index 0)
   *
   * @param message The format string
   * @param parameters The parameters that will replace the format items of the format string
   */
  formatByFormatString(message, parameters) {
    for (let i = 0; i < parameters.length; i++) {
      message = message.replace(`%${i}`, parameters[i]);
    }

    return message;
  }

  /**
   * Fetches the format string from the type object retrieved by passing the notification type to getNotificationType(), then replaces
   * its format items by the given parameters (e.g. format item %0 will be replaced by parameter with index 0)
   *
   * @param type The notification type
   * @param parameters The parameters that will replace the format items of the format string
   */
  getNotificationMessage(type, parameters) {
    const message = this.getNotificationType(type).message;

    return this.formatByFormatString(message, parameters);
  }

  /**
   * Retrieves the type object containing some essential data (format string, button that go with this type of notification, title) by
   * a notification's type
   *
   * @param type The notification type
   */
  getNotificationType(type) {
    return this.notificationTypes.find((element) => element.type === type);
  }
}
