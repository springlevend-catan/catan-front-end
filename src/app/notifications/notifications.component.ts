import {Component, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocalStorageService} from '../service/local-storage-service/local-storage.service';
import {NotificationHandler} from './notification-handler';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationsComponent {
  constructor(private localStorageService: LocalStorageService, private route: ActivatedRoute, private notificationHandler: NotificationHandler) {
    const token = this.localStorageService.getItem('token');

    if (token === this.route.snapshot.params.token) {
      const url = 'http://localhost:5500/notification-socket';
      const socket = new SockJS(`${url}?token=${token}`);
      const stompClient = Stomp.over(socket);
      const that = this;

      stompClient.connect({}, function (frame) {
        stompClient.subscribe(`/notifications/${token}`, (r) => {
          const json = JSON.parse(r.body);

          const notificationContainer = $(`<div class="notification"></div>`);
          const showButtons = true;
          const buttonClassNames = ['notification-button'];

          const notification = that.notificationHandler.createNotificationElement(
            notificationContainer,
            json,
            showButtons,
            buttonClassNames
          );

          $('.notification-container').append(notification);

          setTimeout(() => {
            notification.remove();
          }, 10000);
        });
      });
    }
  }
}
