import {Component, OnInit} from '@angular/core';
import {LobbyService} from '../../service/lobbyService/lobby.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lobbylist',
  templateUrl: './lobbylist.component.html',
  styleUrls: ['./lobbylist.component.scss']
})
export class LobbylistComponent implements OnInit {

  private lobbies: Object = [];
  private amountOfPlayersInLobby;

  constructor(private lobbyService: LobbyService, private router: Router) {
  }

  ngOnInit() {
    this.lobbyService.getAvailableLobbies().subscribe(x => {this.lobbies = x; console.log(this.lobbies); });
  }

  joinLobby(url: string) {
    console.log('joining');
    this.router.navigateByUrl('/lobby/' + url);
  }

}
