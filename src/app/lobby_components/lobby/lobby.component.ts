import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as SockJS from 'sockjs-client';
import {Stomp} from '@stomp/stompjs';
import {Player} from '../../model/player';
import {LobbyService} from '../../service/lobbyService/lobby.service';
import {AuthenticationService} from '../../authentication/service/authenticationService/authentication.service';
import {NotificationService} from '../../service/notification-service/notification.service';
import {LocalStorageService} from '../../service/local-storage-service/local-storage.service';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  url: String;

  players: Player[] = [];
  username = 'test';
  email = 'test';
  stompClient;
  host;
  connection;
  player = new Player('test', 'test', null);
  yourself;
  isHost: boolean;
  size: number;
  connectedFriends: any;
  searchText;
  gameUrl: any;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private lobbyService: LobbyService, private router: Router, private authService: AuthenticationService, private localStorageService: LocalStorageService, private notificationService: NotificationService) {
  }

  ngOnInit() {
    // Get paramater from the url
    this.url = this.route.snapshot.paramMap.get('url');
    console.log('init');

    // Create new Socket
    const socket = new SockJS('http://localhost:5500/ws');
    this.stompClient = Stomp.over(socket);
    const that = this;
    console.log('create socket');
    // Connect the socket to the correct endpoint
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);

      that.stompClient.subscribe('/topic/lobby/' + that.url, function (msg) {
        console.log('received');
        console.log(msg.body);
        that.host = JSON.parse(msg.body).host;
        that.size = JSON.parse(msg.body).size;
        that.gameUrl = JSON.parse(msg.body).gameUrl;

        that.players = JSON.parse(msg.body).users;
        that.players.push(that.host);

        that.checkIfThisPlayerIsHost();
        that.checkIfGameHasStarted();

      });

      that.sendPlayer();
    });
  }

  showInvitationOverlay() {
    $('.invitation-overlay').css('visibility', 'visible');

    this.httpClient.get(`http://localhost:5500/user/${this.localStorageService.getItem('token')}/connected-friends`).subscribe((r) => {
      this.connectedFriends = r;
    });
  }

  sendInvitation(username) {
    this.httpClient.post(`http://localhost:5500/user/${this.localStorageService.getItem('token')}/invite-to-lobby?url=${this.url}`, username, {responseType: 'text'}).subscribe();
  }

  addPlayer(msg) {
    this.players.push(msg);
  }

  onclose() {
    this.leave();
  }

  onerror() {
    this.leave();
  }

  leave() {
    const token = 'Bearer ' + this.localStorageService.getItem('token');
    console.log(token);
    this.stompClient.send('/app/lobby/leave/' + this.url, {}, token);
    // TODO: wait for stompClient to answer to navigate
    this.router.navigateByUrl('/lobbyList');

  }

  sendPlayer() {
    const token = 'Bearer ' + this.localStorageService.getItem('token');
    console.log(token);
    this.player = new Player(this.username, this.email, null);
    this.stompClient.send('/app/lobby/' + this.url, {}, token);
  }

  start() {
    this.lobbyService.startLobby(this.url).subscribe(r => {
      this.router.navigateByUrl('/game/' + r);
    });
  }

  checkIfThisPlayerIsHost() {
    this.authService.getUserWithToken().subscribe(x => {
      this.yourself = x as Player;
      this.isHost = this.yourself.username === this.host.username;
      console.log('HOST : ' + this.isHost);
    });
  }

  addComputerPlayer() {
    this.stompClient.send('/app//lobby/joinComputer/' + this.url, {});
  }

  calculateFreePlayerSpots() {
    return new Array(this.size - this.players.length);
  }


  checkIfGameHasStarted() {
    if (this.gameUrl !== null) {
      this.router.navigateByUrl('/game/' + this.gameUrl);
    }
  }

}
