import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthenticationService } from '../service/authenticationService/authentication.service';
import { User } from '../../../model/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})

export class RegistrationComponent implements OnInit {
   user: User;
   passwordRepeatText: string;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.user = new User();
  }

  registerUser() {
    this.authenticationService.registerUser(this.user);
  }
}
