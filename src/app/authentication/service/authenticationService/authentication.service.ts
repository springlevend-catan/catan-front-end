import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from '../../../../model/user';
import {GlobalVariables} from '../../../../globals';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';
import {TranslateService} from '../../../service/translateService/translate.service';
import {LocalStorageService} from '../../../service/local-storage-service/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public currentUserValue;
  private authenticationUrl;
  private gameLogicUrl;
  private registrationUrl;
  private loginUrl;
  private testApiUrl;
  private getUserUrl;
  private updateUserUrl;
  private getUserByUsernameUrl;

  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router, private translateService: TranslateService, private localStorageService: LocalStorageService) {
    this.authenticationUrl = GlobalVariables.AuthenticationUrl;
    this.registrationUrl = this.authenticationUrl + '/users/sign-up';
    this.loginUrl = this.authenticationUrl + '/users/sign-in';
    this.testApiUrl = this.authenticationUrl + '/test/user';
    this.getUserUrl = this.authenticationUrl + '/users/getByToken';
    this.updateUserUrl = this.authenticationUrl + '/users/updateUser';
    this.getUserByUsernameUrl = this.authenticationUrl + '/users/getByUsername';
  }

  registerUser(user: User) {
    console.log(this.registrationUrl);
    const result = this.http.post(this.registrationUrl, user, {responseType: 'json'});
    result.subscribe(x => {
        console.log('registered');
      },
      error1 => {
        switch (error1.error) {
          case 'Email already in use':
            this.toastr.error('Email already in use', 'ERROR');
            break;
          case 'Username already in use':
            this.toastr.error('Username already in use', 'ERROR');
            break;
          default :
            this.toastr.error('Internal server error', 'ERROR');
        }
      },
      () => {
        this.toastr.success('Registration complete, please confirm your email', 'SUCCES');
      }
    );

  }

  loginUser(userToLogin: User) {
    const result = this.http.post(this.loginUrl, userToLogin, {responseType: 'text'});
    result.subscribe((response) => {
        this.localStorageService.setItem('token', response.valueOf().toString());
        this.router.navigateByUrl('/navigation');
      },
      error1 => {
      console.log(error1);
        switch (error1.error) {
          case 'Email not confirmed':
            this.toastr.error('Please confirm your email', 'ERROR');
            break;
          default:
            this.toastr.error('Bad credentials', 'ERROR');
        }
      },
      () => {
        localStorage.setItem('Username', userToLogin.username);
      }
    );
  }

  getUserWithToken(): Observable<any> {
    const token = this.localStorageService.getItem('token');
    return this.http.get(this.getUserUrl + '/' + token, {responseType: 'json'});
  }

  isLoggedIn(): boolean {
    this.currentUserValue = this.localStorageService.getItem('token');
    return this.currentUserValue;
  }

  updateUser(updateUser: User) {
    const result = this.http.post(this.updateUserUrl, updateUser, {responseType: 'text'});
    result.subscribe((response) => {
      },
      error1 => {
      },
      () => this.toastr.success('Profile updated', 'SUCCESS'));
  }

  logout() {
    this.localStorageService.removeItem('token');
    this.router.navigateByUrl('/');
    this.toastr.success('Logout succesfull', 'Logged out');
    this.currentUserValue = null;
  }

  getUserByUserName(username: string): any {
    return this.http.get(this.getUserByUsernameUrl + '/' + username, {responseType: 'json'});
  }
}
