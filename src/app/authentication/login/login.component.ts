import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../service/authenticationService/authentication.service';
import {User} from '../../../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
   userToLogin: User;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.userToLogin = new User();
  }

  loginUser() {
    this.authenticationService.loginUser(this.userToLogin);
  }
}
