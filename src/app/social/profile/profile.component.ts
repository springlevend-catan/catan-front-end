import {Component, OnInit} from '@angular/core';
import {User} from '../../../model/user';
import {AuthenticationService} from '../../authentication/service/authenticationService/authentication.service';
import {NotificationService} from '../../service/notification-service/notification.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(private authenticationService: AuthenticationService, private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authenticationService.getUserWithToken().subscribe(
      res => {
        console.log('result');
        console.log(res);
        this.user = res;
      },
      error1 => console.log(error1)
    );
  }

  updateUser() {
    this.authenticationService.updateUser(this.user);
  }

  getImageSource(): String {
    let source;
    switch (this.user.profilePicture) {
      case 0:
        source = '../../assets/avatars/avatar0.png';
        break;
      case 1:
        source = '../../assets/avatars/avatar1.png';
        break;
      case 2:
        source = '../../assets/avatars/avatar2.png';
        break;
      case 3:
        source = '../../assets/avatars/avatar3.png';
        break;
      case 4:
        source = '../../assets/avatars/avatar4.png';
        break;
      case 5:
        source = '../../assets/avatars/avatar5.png';
        break;
      case 6:
        source = '../../assets/avatars/avatar6.png';
        break;
      case 7:
        source = '../../assets/avatars/avatar7.png';
        break;
      case 8:
        source = '../../assets/avatars/avatar8.png';
        break;
      case 9:
        source = '../../assets/avatars/avatar9.png';
        break;
      default:
        source = '../../assets/avatars/avatar0.png';
    }
    return source;
  }

  nextProfilePicture() {
    if (this.user.profilePicture === 9) {
      this.user.profilePicture = 0;
    } else {
      this.user.profilePicture = this.user.profilePicture + 1;
    }
    console.log(this.user.profilePicture);
  }

  previousProfilePicture() {
    if (this.user.profilePicture === 0) {
      this.user.profilePicture = 9;
    } else {
      this.user.profilePicture = this.user.profilePicture - 1;
    }
    console.log(this.user.profilePicture);
  }
}
