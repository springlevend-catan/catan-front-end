import {Component, OnInit} from '@angular/core';
import {UserService} from '../../service/userService/user.service';
import {AuthenticationService} from '../../authentication/service/authenticationService/authentication.service';
import {Player} from '../../model/player';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {

  token;

  constructor(private userService: UserService, private authService: AuthenticationService, private httpClient: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  friends: Player[];
  username: String;
  users: Player[];
  user: any;

  ngOnInit() {
    this.authService.getUserWithToken().subscribe(x => {
      this.user = x;
    });
  }

  searchUsers(username: String) {
    this.username = username;
    console.log(username);
    if (this.username.length > 0) {
      this.userService.getUsersFilteredByUsername(this.username).subscribe(x => {
        this.users = x as Player[];
      });
    }
  }

  addFriend(username: String) {
    if (username.replace(/\s/g, '').length) {
      this.httpClient.post(`http://localhost:5500/user/${this.token}/send-friend-request`, username).subscribe((r) => {
        /*
                if (r['status'] === 'fail') {
                  const error = $('.error');

                  error.css('visibility', 'visible');
                  error.text(r['data']['error']);
                }*/
        console.log('adding friend response');
        console.log(r);
      });
    }
  }

}
