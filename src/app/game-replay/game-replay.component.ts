import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from '../service/gameService/game.service';
import {LobbyService} from '../service/lobbyService/lobby.service';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {Action} from '../model/action';

@Component({
  selector: 'app-game-replay',
  templateUrl: './game-replay.component.html',
  styleUrls: ['./game-replay.component.scss']
})
export class GameReplayComponent implements OnInit {

  game = null;
  stompClient;
  url;
  isBuildingCity: boolean;
  token;
  colourArray;
  player;
  hideBuildActions: boolean;
  replayHasStarted = false;

  constructor(private route: ActivatedRoute, private lobbyService: LobbyService, private router: Router, private gameService: GameService) {
  }

  ngOnInit() {
    this.url = this.route.snapshot.paramMap.get('url');
    const socket = new SockJS('http://localhost:5500/ws');
    this.colourArray = ['#FFFFFF', '#FFFF00', '#00FF00', '#0000FF'];
    this.stompClient = Stomp.over(socket);
    console.log('create socket');
    const that = this;
    if (!this.replayHasStarted) {
      this.game = this.gameService.startReplay(this.url).subscribe(x => {
        console.log('GAME WORDT AFGEDRUKT');
        console.log(x);
        this.game = x;
        console.log(this.game);
        this.player = this.getPlayerData(localStorage.getItem('Username'));
        console.log('PLAYER : ' + this.player);

        this.joinPlayer();
        this.replayHasStarted = true;
      });

    }
    // Connect the socket to the correct endpoint
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);
      that.stompClient.subscribe('/topic/game/' + that.url, function (msg) {
        that.game = JSON.parse(msg.body);
        that.player = that.getPlayerData(localStorage.getItem('Username'));
        console.log(that.game);
      });
    });

  }
  private joinPlayer() {
    this.stompClient.send('/app/game/join/' + this.url, {}, this.token);
    console.log('hier loopt het fout');
  }


  getPlayerData(user: string) {
    console.log('player wordt opgehaald');
    return this.game.players.filter(p => p.username === user)[0];
  }


  getNextAction() {
    this.gameService.getNextAction(this.game, this.url).subscribe(x => {
      if (x !== null) {
        this.game = x;
        console.log('resultaat : ' );
        console.log(x);
      }
    });
  }
}
