import {Component, OnInit} from '@angular/core';
import {GameService} from '../service/gameService/game.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Game} from '../model/game';

@Component({
  selector: 'app-gamelist',
  templateUrl: './gamelist.component.html',
  styleUrls: ['./gamelist.component.scss']
})
export class GamelistComponent implements OnInit {
  url;
  game: Game[] = [];
  dataLoaded = false;


  constructor(private gameService: GameService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.url = this.route.snapshot.paramMap.get('url');
  }

  getGamesForList() {
    this.dataLoaded = false;
    this.gameService.getGames().subscribe(x => {
      // @ts-ignore
      for (let i = 0; i < x.length; i++) {
        this.game[i] = new Game();
        this.game[i].players = [];
        x[i].players.forEach((player, index) => {
          this.game[i].players[index] = player.user.username;
        });
        this.game[i].url = x[i].url;
      }
      console.log(x);
      this.dataLoaded = true;
    });
  }
  startReplay(url: string) {
    console.log('going to start replay from game with url : ' + url);
    this.router.navigateByUrl('gameReplay/' + url);
  }
}
