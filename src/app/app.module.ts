import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {HexComponent} from './game_components/board_components/hex/hex.component';
import {BoardComponent} from './game_components/board_components/board/board.component';
import {RouterModule, Routes} from '@angular/router';
import {ChatComponent} from './game_components/chat/chat.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BuildingSpotComponent} from './game_components/board_components/building-spot/building-spot.component';
import {RegistrationComponent} from './authentication/registration/registration.component';
import {LoginComponent} from './authentication/login/login.component';
import {NavigationComponent} from './navigation/navigation.component';
import {DiceComponent} from './game_components/dice/dice.component';
import {LobbyComponent} from './lobby_components/lobby/lobby.component';
import {LobbylistComponent} from './lobby_components/lobbylist/lobbylist.component';
import {PlayerComponent} from './game_components/player/player.component';
import {AuthInterceptor} from '../AuthInterceptor';
import {AuthGuard} from './authentication/security/auth-guard';
import {registerLocaleData} from '@angular/common';
import localeNl from '@angular/common/locales/fr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {TranslateService} from './service/translateService/translate.service';
import {TranslatePipe} from './translate.pipe';
import {GameComponent} from './game_components/game/game.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ProfileComponent} from './social/profile/profile.component';
import {LoaderComponent} from './loader/loader.component';
import {FriendsComponent} from './social/friends/friends.component';
import {PlayersComponent} from './game_components/players/players.component';
import {DevelopmentCardComponent} from './game_components/development-card/development-card.component';
import {InventionCardComponent} from './game_components/invention-card/invention-card.component';
import {ResourceCardComponent} from './game_components/resource-card/resource-card.component';
import { GamelistComponent } from './gamelist/gamelist.component';
import { GameReplayComponent } from './game-replay/game-replay.component';
import {RulesComponent} from './game_components/rules/rules.component';
import {TradeComponent} from './game_components/trade/trade.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {TestNotificationsComponent} from './test-notifications/test-notifications.component';
import { StealComponent } from './game_components/steal/steal.component';
import {FilterPipe} from './lobby_components/lobby/filter-pipe';


registerLocaleData(localeNl, 'nl');

export function setupTranslateFactory(
  service: TranslateService): Function {

  return () => service.use('en');
}

const appRoutes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'notifications/:token', component: NotificationsComponent, outlet: 'notifications'}, {
    path: 'game/test', component: GameComponent
  },
  {
    path: 'game/:url', component: GameComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'gameReplay/:url', component: GameReplayComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'chat', component: ChatComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'gameList', component: GamelistComponent, canActivate: [
      AuthGuard
    ]
  },
  {path: 'register', component: RegistrationComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'navigation', component: NavigationComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'dice', component: DiceComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'lobby/:url', component: LobbyComponent
  },
  {path: 'test-notifications', component: TestNotificationsComponent},
  {
    path: 'profile', component: ProfileComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'rules', component: RulesComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'trade', component: TradeComponent, canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'lobbyList', component: LobbylistComponent, canActivate: [AuthGuard]
  },

  {
    path: 'friends', component: FriendsComponent, canActivate: [
      AuthGuard
    ]
  },
  {path: 'chat/:id', component: ChatComponent, canActivate: [AuthGuard]},
  {
    path: '**', redirectTo: '/notFound',
  },
  {
    path: 'notFound', component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HexComponent,
    BoardComponent,
    ChatComponent,
    BuildingSpotComponent,
    RegistrationComponent,
    LoginComponent,
    NavigationComponent,
    DiceComponent,
    LobbyComponent,
    LobbylistComponent,
    GameComponent,
    PlayerComponent,
    TranslatePipe,
    NotFoundComponent,
    ProfileComponent,
    LoaderComponent,
    FriendsComponent,
    PlayersComponent,
    ResourceCardComponent,
    RulesComponent,
    TradeComponent,
    GamelistComponent,
    GameReplayComponent,
    RulesComponent,
    DevelopmentCardComponent,
    InventionCardComponent,
    NotificationsComponent,
    TestNotificationsComponent,
    StealComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,

  },
    /* ErrorInterceptor, {
       provide: HTTP_INTERCEPTORS,
       useClass: ErrorInterceptor,
       multi: true
     },*/
    TranslateService, {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
