import {Component, OnInit} from '@angular/core';
import {LobbyService} from '../service/lobbyService/lobby.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication/service/authenticationService/authentication.service';
import {NotificationService} from '../service/notification-service/notification.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private lobbyService: LobbyService, private authenticationService: AuthenticationService, private router: Router, private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  createNewLobby() {
    console.log('click');
    this.lobbyService.createNewLobby().subscribe(r => {
      this.router.navigateByUrl('/lobby/' + r);
      console.log(r);
    });
  }

  logout() {
    this.authenticationService.logout();
  }

}
