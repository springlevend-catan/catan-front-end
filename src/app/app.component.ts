import { Component } from '@angular/core';
import {TranslateService} from './service/translateService/translate.service';
import {NotificationService} from './service/notification-service/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(private translate: TranslateService, /*private notificationService: NotificationService*/) {}

  setLang(lang: string) {
    this.translate.use(lang);
  }
}
