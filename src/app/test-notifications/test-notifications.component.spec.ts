import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestNotificationsComponent } from './test-notifications.component';

describe('TestNotificationsComponent', () => {
  let component: TestNotificationsComponent;
  let fixture: ComponentFixture<TestNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
