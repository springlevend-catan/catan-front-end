import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocalStorageService} from '../service/local-storage-service/local-storage.service';
import {NotificationService} from '../service/notification-service/notification.service';
import * as $ from 'jquery';
import {NotificationHandler} from '../notifications/notification-handler';

@Component({
  selector: 'app-test-notifications',
  templateUrl: './test-notifications.component.html',
  styleUrls: ['./test-notifications.component.scss']
})
export class TestNotificationsComponent {
  token;
  friends;
  notifications;

  constructor(private httpClient: HttpClient, private localStorageService: LocalStorageService, private notificationService: NotificationService, private notificationHandler: NotificationHandler) {
    this.token = localStorageService.getItem('token');

    this.setFriends();
    this.setNotifications();
  }

  sendFriendRequest(username) {
    if (username.replace(/\s/g, '').length) {
      this.httpClient.post(`http://localhost:5500/user/${this.token}/send-friend-request`, username, {responseType: 'text'}).subscribe((r) => {
        console.log(r);
      });
    }
  }

  setNotifications() {
    this.httpClient.get(`http://localhost:5500/user/${this.token}/notifications`).subscribe((r: any[]) => {
        this.notifications = [];

        for (let i = 0; i < r.length; i++) {
          this.notifications[i] = this.notificationHandler.getNotificationMessage(r[i].type, r[i].parameters);
        }
      }
    );
  }

  setFriends() {
    this.httpClient.get(`http://localhost:5000/user/${this.token}/friends`).subscribe((r: any[]) => {
      this.friends = [];

      for (let i = 0; i < r.length; i++) {
        this.friends[i] = r[i].username;
      }
    });
  }
}
