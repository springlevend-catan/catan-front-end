import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GlobalVariables} from '../../../globals';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUser(username: string): Observable<any> {
    return this.http.get(GlobalVariables.FrontEndLink + '/api/user/getfriends');
  }
  getUsersFilteredByUsername(username: String) {
    return this.http.get(GlobalVariables.AuthenticationUrl + '/users/get/' + username);
  }

}
