import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Player} from '../../model/player';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {ActivatedRoute} from '@angular/router';
import {Action} from '../../model/action';
import {LocalStorageService} from '../local-storage-service/local-storage.service';


@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  private url = 'http://localhost:5500/api/';
  constructor(private route: ActivatedRoute, private http: HttpClient, private localStorageService: LocalStorageService) {

    const socket = new SockJS('http://localhost:5500/ws');
    this.stompClient = Stomp.over(socket);
    this.socketUrl = this.route.snapshot.paramMap.get('url');
    const that = this;
    // Connect the socket to the correct endpoint
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);
      that.stompClient.subscribe('/lobby/join/' + that.socketUrl, function (msg) {
        console.log((msg.body));
      });
    });

  }
  private stompClient;


  private socketUrl;

  createNewLobby() {
    return this.http.post(this.url + 'lobby/create', (new Player('test', 'test', null)), this.httpOptions);
  }

  getAvailableLobbies() {
    console.log('do call');
    return this.http.get(this.url + 'lobby/available');
  }

  startLobby(url: String) {
    return this.http.post(this.url + 'lobby/start/' + url, ' ', this.httpOptions);
  }

  joinLobby(url: String) {
    this.stompClient.send('/lobby/join/' + url, {}, this.localStorageService.getItem('token'));
  }
}
