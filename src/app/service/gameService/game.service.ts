import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  private url = 'http://localhost:5500/api/';

  constructor(private http: HttpClient) {
  }

  getBuildingSpots(url: String) {
    return this.http.get(this.url + 'game/villageSpots/' + url);
  }

  getStreetSpots(url: String) {
    return this.http.get(this.url + 'game/streetSpots/' + url);
  }

  getStealPlayers(url: String) {
    return this.http.get(this.url + 'game/steal/' + url);
  }

  getGames() {
    return this.http.get(this.url + 'games');
  }

  startReplay(url: string) {
    console.log('surfing to ' + this.url + 'replay/startReplay/' + url);
    return this.http.get(this.url + 'replay/startReplay/' + url);
  }
// returns next action of this game
  getNextAction(game: any, url) {
    const turnCounter = game.turnCounter;
    console.log(game);
    return this.http.post(this.url + 'replay/next/' + url, game);
  }
}
