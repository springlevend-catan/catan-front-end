import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public onLocalStorageChanged = new Subject();

  setItem(key, value) {
    localStorage.setItem(key, value);

    this.onLocalStorageChanged.next();
  }

  getItem(key) {
    return localStorage.getItem(key);
  }

  removeItem(key) {
    localStorage.removeItem(key);

    this.onLocalStorageChanged.next();
  }

  clear() {
    localStorage.clear();

    this.onLocalStorageChanged.next();
  }
}
