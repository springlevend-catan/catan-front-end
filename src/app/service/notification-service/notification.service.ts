import {Injectable} from '@angular/core';
import {LocalStorageService} from '../local-storage-service/local-storage.service';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private token = this.localStorageService.getItem('token');

  constructor(private localStorageService: LocalStorageService, private router: Router) {
    if (this.token) { // If user is logged in, pull up Notifications component
      this.setNotificationsOutlet();
    } else { // If user isn't logged in, subscribe to changes on localStorage
      this.subscribeToChanges();
    }
  }

  subscribeToChanges() {
    this.localStorageService.onLocalStorageChanged.subscribe(() => {
      this.token = this.localStorageService.getItem('token');

      if (this.token) { // If user logs in, pull up Notifications component
        this.setNotificationsOutlet();
      } else { // If user logs out, clear Notifications component, thus killing the WebSocket connection
        this.clearNotificationsOutlet();
      }
    });
  }

  setNotificationsOutlet() {
    // Set Notifications component in 'notifications' router outlet
    this.router.navigate([{outlets: {notifications: `notifications/${this.token}`}}]);
  }

  clearNotificationsOutlet() {
    // Remove Notifications component from 'notifications' router outlet
    this.router.navigate([{outlets: {notifications: null}}]);
  }
}
