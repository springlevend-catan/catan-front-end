import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http: HttpClient) {
  }

  getBoard(): any {
    return this.http.get('http://localhost:5000/api/board/new', {responseType: 'json'});
  }
}

