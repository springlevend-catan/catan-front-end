// The state for the loader
export interface LoaderState {
  show: boolean;
}
