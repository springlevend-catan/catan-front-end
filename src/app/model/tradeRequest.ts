export class TradeRequest {
   resourcesToGive = [];
   resourcesToGet = [];
   username: String;


  constructor(resourcesToGive: any[], username: String, resourcesToGet: any[]) {
    this.resourcesToGive = resourcesToGive;
    this.resourcesToGet = resourcesToGet;
    this.username = username;
  }
}
