export class Action {
  public token: string;

  public type: string;

  public value: string;

  constructor(token: string, type: string, value: string) {
    this.token = token;
    this.type = type;
    this.value = value;
  }
}
