export class Player {
  public username: string;
  public email: string;
  public friends: Player[];

  constructor(username: string, email: string, friends: Player[]) {
    this.username = username;
    this.email = email;
    this.friends = friends;
  }
}
