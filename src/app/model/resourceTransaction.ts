export class ResourceTransaction {
  playerFrom: string;
  playerTo: string;
  receivedResources = [];
  removedResources = [];


  constructor(playerFrom: string, playerTo: string, receivedResources: any[], removedresources: any[]) {
    this.playerFrom = playerFrom;
    this.playerTo = playerTo;
    this.receivedResources = receivedResources;
    this.removedResources = removedresources;
  }
}
