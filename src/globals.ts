export const GlobalVariables = Object.freeze({
   AuthenticationUrl: 'http://localhost:5000',
   GameLogicUrl: 'http://localhost:5500',
   DutchTranslateLink: 'http://localhost:4200/assets/i18n/nl.json',

   EnglishTranslateLink: 'http://localhost:4200/assets/i18n/en.json',
   FrontEndLink: 'http://localhost:4200'

   // AuthenticationUrl: 'http://catan-authentication-unexpected-alligator.cfapps.io',
   // GameLogicUrl: 'http://catan-game-logic.herokuapp.com/',
   // DutchTranslateLink: 'http://catan-stage.surge.sh/catan-stage.surge.sh/assets/i18n/nl.json',
   // EnglishTranslateLink: 'http://catan-stage.surge.sh/catan-stage.surge.sh/assets/i18n/en.json'

   // FrontEndLink: 'FILL IN'
});

// TODO: fill in correct front end link
