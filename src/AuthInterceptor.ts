import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoaderService} from './app/service/loaderService/loader.service';
import {tap} from 'rxjs/operators';
import {LocalStorageService} from './app/service/local-storage-service/local-storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private loaderService: LoaderService, private localStorageService: LocalStorageService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // On http request start to show the loader
    this.loaderService.show();

    if (req.url !== 'http://localhost:5000/users/sign-up' && req.url !== 'http://localhost:5000/users/sign-in') {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.localStorageService.getItem('token')}`
        }
      });
    }
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // When request finished, hide the loader
          this.onEnd();
        }
      },
      (err: any) => {
      // When an error occures, hide the loader
        this.onEnd();
      }));
  }

  private onEnd(): void {
    this.hideLoader();
  }

  // Call the loader service when needed
  private showLoader(): void {
    this.loaderService.show();
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }


}
